from django.shortcuts import render
from datetime import datetime, date

# My Name
mhs_name = 'Clouddian Fazalmuttaqin'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 6, 10)
npm = 1706984543 # TODO
college_place = 'Universitas Indonesia'
hobby = 'I love reading a novel, watching a movie, and sometimes playing an online game'
description = 'A dedicated student with proven analysis and problem-solving skills, \
highly interested in business and IT development.'

# My Friend1 Name
name_friend1 = 'Julia Ningrum'
curr_year = int(datetime.now().strftime("%Y"))
birth_date_friend1 = date(2000, 7, 27)
npm_friend1 = 1706979322
college_place_friend1 = 'Universitas Indonesia'
hobby_friend1 = 'Gaming'
description_friend1 = 'Saya biasa dipanggil "Juli". Saya lahir pada tanggal 27 Juli 2000. \
Saya jurusan Ilmu Komputer.'

# My Friend2 Name
name_friend2 = 'Muhamad Fariz Farhan'
curr_year = int(datetime.now().strftime("%Y"))
birth_date_friend2 = date(1998, 12, 8)
npm_friend2 = 1706043380
college_place_friend2 = 'Universitas Indonesia'
hobby_friend2 = 'Tidur'
description_friend2 = 'saya adalah orang yang seru dan bersemangat'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 
    'hobby': hobby, 'place' : college_place, 'description' : description,
    'name_friend1': name_friend1, 'age_friend1': calculate_age(birth_date_friend1.year), 'npm_friend1': npm_friend1, 
    'hobby_friend1': hobby_friend1, 'place_friend1' : college_place_friend1, 'description_friend1' : description_friend1,
    'name_friend2': name_friend2, 'age_friend2': calculate_age(birth_date_friend2.year), 'npm_friend2': npm_friend2, 
    'hobby_friend2': hobby_friend2, 'place_friend2' : college_place_friend2, 'description_friend2' : description_friend2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
