from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'index_lab3_home.html')

def about(request):
    return render(request, 'index_lab3_about.html')

def work(request):
    return render(request, 'index_lab3_work.html')

def login(request):
    return render(request, 'index_lab3_login.html')