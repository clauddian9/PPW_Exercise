from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Schedule
from .forms import Schedule_Form

# Create your views here.
response = {}
def home(request):
    return render(request, 'index_lab4_home.html')

def about(request):
    return render(request, 'index_lab4_about.html')

def work(request):
    return render(request, 'index_lab4_work.html')

def login(request):
    return render(request, 'index_lab4_login.html')

def schedule_list(request):
    response['schedule_list'] = Schedule.objects.all()
    return render(request, 'index_story5_schedule_list.html', response)

def schedule_new(request):
    form = Schedule_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['day'] = request.POST['day']
        response['date_time'] = request.POST['date_time']
        response['hours'] = request.POST['hours']
        response['activities_name'] = request.POST['activities_name']
        response['location'] = request.POST['location']
        response['category'] = request.POST['category']
        schedule = Schedule(day=response['day'], date_time = response['date_time'], 
                            hours=response['hours'], activities_name=response['activities_name'],
                            location = response['location'], category=response['category'])
        schedule.save()
        return HttpResponseRedirect('/schedule_list/')
    else:
        return HttpResponseRedirect('/home/')

def schedule_add(request):
    response['add_schedule'] = Schedule_Form()
    return render(request, 'index_story5_schedule_add.html', response)

def schedule_delete(request):
    Schedule.objects.all().delete()
    return HttpResponseRedirect('/schedule_list/')
