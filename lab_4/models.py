from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Schedule(models.Model):
    day = models.CharField(max_length=40)
    date_time = models.DateField()
    hours = models.TimeField()
    activities_name = models.CharField(max_length=50)
    location = models.CharField(max_length=50)
    category = models.CharField(max_length=50)