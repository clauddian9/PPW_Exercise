from django.urls import path

from lab_4.views import home
from lab_4.views import about
from lab_4.views import work
from lab_4.views import login
from lab_4.views import schedule_list
from lab_4.views import schedule_new
from lab_4.views import schedule_add
from lab_4.views import schedule_delete

urlpatterns = [
    path('', home, name='home'),
    path('home/', home, name='home'),
    path('about/', about, name='about'),
    path('work/', work, name='work'),
    path('login/', login, name='login'),
    path('schedule_list/',schedule_list, name='schedule_list'),
    path('schedule_new/', schedule_new, name='schedule_new'),
    path('schedule_add/', schedule_add, name='schedule_add'),
    path('schedule_delete/', schedule_delete, name='schedule_delete')
]
