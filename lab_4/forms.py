from django import forms

class Schedule_Form(forms.Form):
    error_messages = {
        'required': 'Please fill this field',
    }
    attrs_text = {
        'class': 'form-control'
    }
    attrs_date = {
        'class': 'form-control',
        'type' : 'date'
    }

    day = forms.ChoiceField(
        label = 'Day', 
        choices = [("Senin", "Senin"), ("Selasa", "Selasa"), ("Rabu", "Rabu"),
                   ("Kamis", "Kamis"), ("Jumat","Jumat"), ("Sabtu", "Sabtu"), ("Minggu", "Minggu")], 
        required = True,
        widget = forms.Select(attrs = attrs_text))

    date_time = forms.DateField(
        label = 'Date', 
        required = True,
        widget = forms.DateInput(attrs = attrs_date))
    
    hours = forms.TimeField(
        label = 'Hours', 
        input_formats = ['%H:%M'],
        required=True,
        widget = forms.TimeInput(format = '%H:%M', attrs = attrs_text))

    activities_name = forms.CharField(
        label = 'Activities', 
        required = True, 
        max_length = 50, 
        widget = forms.TextInput(attrs = attrs_text))

    location = forms.CharField(
        label = 'Location', 
        required = True, 
        max_length = 50, 
        widget = forms.TextInput(attrs = attrs_text))

    category = forms.CharField(
        label='Category', 
        required=True, 
        max_length= 50, 
        widget=forms.TextInput(attrs = attrs_text))

