"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import include, path
from django.contrib import admin
#from lab_1.views import index as index_lab1
from lab_4.views import home
from lab_4.views import about
from lab_4.views import work
from lab_4.views import login
from lab_4.views import schedule_list
from lab_4.views import schedule_new
from lab_4.views import schedule_add


urlpatterns = [
    path('admin/', admin.site.urls),
    #re_path(r'^lab-1/', include('lab_1.urls')),
    #re_path(r'^lab-2/', include('lab_2.urls')),
    #re_path(r'^$', index_lab1, name='index'),
    path('', include('lab_4.urls')),
]
